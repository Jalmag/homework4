﻿using Homework3.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace Homework3.Controllers
{
    class MovieController
    {
        private string path = ConfigurationManager.AppSettings.Get("db");
        private List<Movie> allMovies = new List<Movie>();

        public MovieController()
        {
            try
            {
                Console.WriteLine($"DB path:{path}");
                if (File.Exists(path))
                {
                    string json = File.ReadAllText(path);
                    allMovies = JsonConvert.DeserializeObject<List<Movie>>(json);
                    Console.WriteLine($"Movie database found with {allMovies.Count} movies!");
                }
                else
                {
                    AddDummyMovies();
                    Console.WriteLine("Movie database not found! Creating new database.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error initializing movie database!\n" + e.Message);
                throw;
            }
        }

        public List<Movie> GetMovies()
        {
            return allMovies;
        }

        public Movie GetMovie (int id)
        {
            Movie movie = allMovies.First(x => x.Id == id);
            return movie;
        }

        public void AddMovie(string name, string description, int length)
        {
            int id = GetNewId();
            allMovies.Add(new Movie { Id = id, Name = name, Description = description, Length = length });
            WriteToFile();
        }

        public void RemoveMovie(int id)
        {
            Movie movieToRemove = allMovies.First(x => x.Id == id);
            allMovies.Remove(movieToRemove);
            WriteToFile();
        }

        public List<Movie> GetFilteredMovies(string searchTerm)
        {
            List<Movie> filteredMovies = allMovies.Where(x => x.Name.ToLower().Contains(searchTerm.ToLower())).ToList();
            return filteredMovies;
        }

        private int GetNewId()
        {
            int freeId = 0;
            while (allMovies.Any(x => x.Id == freeId))
            {
                freeId++;
            }
            return freeId;
        }

        private void AddDummyMovies()
        {
            AddMovie("The Godfather", "The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.", 175);
            AddMovie("The Shawshank Redemption ", "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.", 142);
            AddMovie("Schindler's List", "In German-occupied Poland during World War II, industrialist Oskar Schindler gradually becomes concerned for his Jewish workforce after witnessing their persecution by the Nazis.", 195);
            AddMovie("Raging Bull", "The life of boxer Jake LaMotta, whose violence and temper that led him to the top in the ring destroyed his life outside of it.", 129);
            AddMovie("Casablanca", "A cynical American expatriate struggles to decide whether or not he should help his former lover and her fugitive husband escape French Morocco.", 102);
            AddMovie("Citizen Kane", "Following the death of publishing tycoon Charles Foster Kane, reporters scramble to uncover the meaning of his final utterance; 'Rosebud'.", 119);
            AddMovie("Gone with the Wind", "A manipulative woman and a roguish man conduct a turbulent romance during the American Civil War and Reconstruction periods.", 238);
            AddMovie("The Wizard of Oz", "Dorothy Gale is swept away from a farm in Kansas to a magical land of Oz in a tornado and embarks on a quest with her new friends to see the Wizard who can help her return home to Kansas and help her friends as well.", 102);
            AddMovie("One Flew Over the Cuckoo's Nest", "A criminal pleads insanity and is admitted to a mental institution, where he rebels against the oppressive nurse and rallies up the scared patients.", 133);
            AddMovie("Lawrence of Arabia", "The story of T.E. Lawrence, the English officer who successfully united and led the diverse, often warring, Arab tribes during World War I in order to fight the Turks.", 228);
            AddMovie("Vertigo", "A former police detective juggles wrestling with his personal demons and becoming obsessed with a hauntingly beautiful woman.", 128);
            AddMovie("Psycho", "A Phoenix secretary embezzles $40,000 from her employer's client, goes on the run, and checks into a remote motel run by a young man under the domination of his mother.", 109);
            AddMovie("The Godfather: Part II", "The early life and career of Vito Corleone in 1920s New York City is portrayed, while his son, Michael, expands and tightens his grip on the family crime syndicate.", 202);
            AddMovie("On the Waterfront", "An ex-prize fighter turned longshoreman struggles to stand up to his corrupt union bosses.", 108);
            AddMovie("Sunset Blvd.", "A screenwriter develops a dangerous relationship with a faded film star determined to make a triumphant return.", 110);
            AddMovie("Forrest Gump", "The presidencies of Kennedy and Johnson, the events of Vietnam, Watergate and other historical events unfold through the perspective of an Alabama man with an IQ of 75, whose only desire is to be reunited with his childhood sweetheart.", 142);
            AddMovie("The Sound of Music", "A woman leaves an Austrian convent to become a governess to the children of a Naval officer widower.", 172);
            AddMovie("12 Angry Men", "A jury holdout attempts to prevent a miscarriage of justice by forcing his colleagues to reconsider the evidence.", 96);
            AddMovie("West Side Story", "Two youngsters from rival New York City gangs fall in love, but tensions between their respective friends build toward tragedy.", 153);
            AddMovie("Star Wars: Episode IV - A New Hope", "Luke Skywalker joins forces with a Jedi Knight, a cocky pilot, a Wookiee and two droids to save the galaxy from the Empire's world-destroying battle station, while also attempting to rescue Princess Leia from the mysterious Darth Vader.", 121);
            AddMovie("2001: A Space Odyssey", "After discovering a mysterious artifact buried beneath the Lunar surface, mankind sets off on a quest to find its origins with help from intelligent supercomputer H.A.L. 9000.", 149);
            AddMovie("E.T. the Extra-Terrestrial", "A troubled child summons the courage to help a friendly alien escape Earth and return to his home world.", 115);
            AddMovie("The Silence of the Lambs", "A young F.B.I. cadet must receive the help of an incarcerated and manipulative cannibal killer to help catch another serial killer, a madman who skins his victims.", 118);
            AddMovie("Chinatown", "A private detective hired to expose an adulterer finds himself caught up in a web of deceit, corruption, and murder.", 130);
            AddMovie("The Bridge on the River Kwai", "British POWs are forced to build a railway bridge across the river Kwai for their Japanese captors, not knowing that the allied forces are planning to destroy it.", 161);
            AddMovie("The Lord of the Rings: The Return of the King", "Gandalf and Aragorn lead the World of Men against Sauron's army to draw his gaze from Frodo and Sam as they approach Mount Doom with the One Ring.", 201);
            AddMovie("Gladiator", "A former Roman General sets out to exact vengeance against the corrupt emperor who murdered his family and sent him into slavery.", 155);
            AddMovie("Titanic", "A seventeen-year-old aristocrat falls in love with a kind but poor artist aboard the luxurious, ill-fated R.M.S. Titanic.", 194);
            AddMovie("Raiders of the Lost Ark", "In 1936, archaeologist and adventurer Indiana Jones is hired by the U.S. government to find the Ark of the Covenant before Adolf Hitler's Nazis can obtain its awesome powers.", 115);
            AddMovie("Braveheart", "Scottish warrior William Wallace leads his countrymen in a rebellion to free his homeland from the tyranny of King Edward I of England.", 178);
        }

        private void WriteToFile()
        {
            try
            {
                string json = JsonConvert.SerializeObject(allMovies);
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
                File.WriteAllText(path, json);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error writing data to database!\n" + e.Message);
                throw;
            }
        }
    }
}
