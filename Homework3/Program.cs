﻿using Homework3.Controllers;
using Homework3.Entities;
using System;
using System.Collections.Generic;

namespace Homework3
{
    class Program
    {
        static MovieController movieDatabase;

        static void Main(string[] args)
        {
            movieDatabase = new MovieController();
            ShowMenu();
        }

        public static void ShowMenu()
        {
            Console.WriteLine("Welcome to movie manager! Use 'help' to list available commands.");
            string input = "";
            while (input != "quit")
            {
                string userInput = Console.ReadLine();
                userInput = userInput.ToLower();
                string[] commands = userInput.Split(' ');
                input = commands[0];
                switch (input)
                {
                    case "add":
                        AddMovie();
                        break;
                    case "list":
                        bool shortListing = true;
                        try
                        {
                            if (commands[1] == "full")
                            {
                                shortListing = false;
                            }
                        }
                        catch (IndexOutOfRangeException)
                        {

                        }
                        finally
                        {
                            ListMovies(movieDatabase.GetMovies(), shortListing);
                        }
                        break;
                    case "help":
                        ShowHelp();
                        break;
                    case "remove":
                        string id = "";
                        try
                        {
                            id = commands[1];
                        }
                        catch (IndexOutOfRangeException)
                        {
                            Console.WriteLine("Enter a movie id:");
                            id = Console.ReadLine();
                        }
                        finally
                        {
                            Remove(id);
                        }
                        break;
                    case "search":
                        string searchTerm = "";
                        try
                        {
                            searchTerm = commands[1];
                        }
                        catch (IndexOutOfRangeException)
                        {
                            Console.WriteLine("Enter a search term:");
                            searchTerm = Console.ReadLine();
                        }
                        finally
                        {
                            Search(searchTerm);
                        }
                        break;
                    case "quit":
                        break;
                    default:
                        Console.WriteLine("Invalid command! Use 'help' to list available commands. (Without ' ')");
                        break;
                }
            }
        }

        private static void AddMovie()
        {
            Console.WriteLine("Name:");
            string name = Console.ReadLine();
            Console.WriteLine("Description:");
            string description = Console.ReadLine();
            Console.WriteLine("Running time in whole minutes:");
            bool success = false;
            while (!success)
            {
                string length = Console.ReadLine();
                int l;
                success = int.TryParse(length, out l);
                if (success)
                {
                    movieDatabase.AddMovie(name, description, l);
                    Console.WriteLine("Movie added.");
                }
                else
                {
                    Console.WriteLine("Please enter an integer number!");
                }
            }
        }

        private static void ListMovies(List<Movie> moviesToList, bool shortListing)
        {
            for (int i = 0; i < moviesToList.Count; i++)
            {
                if (shortListing)
                {
                    Console.WriteLine($"Id: {moviesToList[i].Id}\t\t{moviesToList[i].Name}");
                }
                else
                {
                    Console.WriteLine($"\nId: {moviesToList[i].Id}\t\t{moviesToList[i].Name}");
                    Console.WriteLine($"\t\t{moviesToList[i].Length} min");
                    Console.WriteLine($"{moviesToList[i].Description}");
                }
            }
        }

        private static void Search(string searchTerm)
        {
            List<Movie> filteredMovies = movieDatabase.GetFilteredMovies(searchTerm);
            Console.WriteLine($"Found {filteredMovies.Count} matches.");
            ListMovies(filteredMovies,false);
        }

        private static void Remove(string id)
        {
            int validId = 0;
            try
            {
                validId = int.Parse(id);
                Movie movieToRemove = movieDatabase.GetMovie(validId);
                Console.WriteLine($"Removing movie {movieToRemove.Name}.");
                if (GetConfirmation())
                {
                    movieDatabase.RemoveMovie(validId);
                    Console.WriteLine("Movie removed from the database.");
                }
                else
                {
                    Console.WriteLine("Removing movie cancelled.");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Movie id is an integer. Use 'list' to find out movie ids.");
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine($"Movie with id {validId} does not exist in the database!");
            }
        }

        private static bool GetConfirmation()
        {
            bool confirmation = false;
            Console.WriteLine("Are you sure? (y/n)");
            string answer = Console.ReadLine();
            answer = answer.ToLower();
            if (answer == "y" || answer == "yes")
            {
                confirmation = true;
            }
            return confirmation;
        }

        private static void ShowHelp()
        {
            Console.WriteLine("Command           Options             Description");
            Console.WriteLine("add                                   Add a new movie to the database.");
            Console.WriteLine("list              full                List all movies. Option full with descriptions and running times.");
            Console.WriteLine("search            <string>            List all movies that have <string> in their name.");
            Console.WriteLine("remove            <id>                Remove movie with id <id> from the database.");
            Console.WriteLine("quit                                  Quit the application.");
        }
    }
}
