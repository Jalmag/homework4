﻿using Homework7.Models;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Homework7.Repositories
{
    public class HighScoreRepository : IHighScoreRepository
    {
        private readonly highscore_dbContext dbContext;

        /// <summary>
        /// Create unique Id
        /// Type <typeparamref name="T"/> needs to have public property 'Id'
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"> 
        /// List of objects of <typeparamref name="T"/>
        /// The method looks at this list and returns an id that the members of this list don't have
        /// </param>
        /// <returns>Created integer Id or 0 if <typeparamref name="T"/> doesn't have 'Id'</returns>
        private int CreateId<T>(object table)
        {
            if (typeof(T).GetProperty("Id") == null )
            {
                return 0;
            }
            int index = 0;
            bool success = false;
            while(!success)
            {
                index++;
                if (!((List<T>)table).Exists(x => (int)typeof(T).GetProperty("Id").GetValue(x) == index))
                {
                    success = true;
                }
            }
            return index;
        }

        public HighScoreRepository(highscore_dbContext context)
        {
            dbContext = context;
        }
        public int AddHighScore(Score score)
        {
            score.TimeStamp = DateTime.Now;
            score.Id = CreateId<Score>(GetAllScores());
            EntityEntry<Score> addedScore = dbContext.Scores.Add(score);
            dbContext.SaveChanges();
            return addedScore.Entity.Id;
        }

        public int AddPlayer(Player player)
        {
            player.Id = CreateId<Player>(GetAllPlayers());
            EntityEntry<Player> newPlayer = dbContext.Players.Add(player);
            dbContext.SaveChanges();
            return (int)newPlayer.Entity.Id;
        }

        public int AddLevel(Level level)
        {
            level.Id = CreateId<Level>(GetAllLevels());
            EntityEntry<Level> newLevel = dbContext.Levels.Add(level);
            dbContext.SaveChanges();
            return newLevel.Entity.Id;
        }

        public void DeleteHighScore(int id)
        {
            Score scoreToRemove = GetHighScore(id);
            dbContext.Scores.Remove(scoreToRemove);
            dbContext.SaveChanges();
        }

        public Score GetHighScore(int id)
        {
            Score highScore = dbContext.Scores.FirstOrDefault(x => x.Id == id);
            return highScore;
        }

        public Level GetLevel(int id)
        {
            Level level = dbContext.Levels.FirstOrDefault(x => x.Id == id);
            return level;
        }

        public Player GetPlayer(int id)
        {
            Player player = dbContext.Players.FirstOrDefault(x => x.Id == id);
            return player;
        }

        public List<Score> GetAllScores()
        {
            List<Score> allScores = dbContext.Scores.ToList();
            return allScores;
        }

        public List<Level> GetAllLevels()
        {
            List<Level> allLevels = dbContext.Levels.ToList();
            return allLevels;
        }

        public List<Player> GetAllPlayers()
        {
            List<Player> allPlayers = dbContext.Players.ToList();
            return allPlayers;
        }
    }
}
