﻿using Homework7.Models;
using System.Collections.Generic;

namespace Homework7.Repositories
{
    public interface IHighScoreRepository
    {
        int AddHighScore(Score score);
        int AddLevel(Level level);
        int AddPlayer(Player player);
        void DeleteHighScore(int id);
        List<Level> GetAllLevels();
        List<Player> GetAllPlayers();
        List<Score> GetAllScores();
        Score GetHighScore(int id);
        Level GetLevel(int id);
        Player GetPlayer(int id);
    }
}