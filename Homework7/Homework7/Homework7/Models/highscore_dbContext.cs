﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Homework7.Models
{
    public partial class highscore_dbContext : DbContext
    {
        public highscore_dbContext()
        {
        }

        public highscore_dbContext(DbContextOptions<highscore_dbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Level> Levels { get; set; }
        public virtual DbSet<Player> Players { get; set; }
        public virtual DbSet<Score> Scores { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Server=PostreSQL 12;Host=localhost;Port=5432;Username=postgres;Password=1234;Database=highscore_db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Level>(entity =>
            {
                entity.ToTable("levels");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Player>(entity =>
            {
                entity.ToTable("players");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CountryCode).HasColumnName("country_code");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnName("user_name")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Score>(entity =>
            {
                entity.ToTable("scores");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.HighScore).HasColumnName("high_score");

                entity.Property(e => e.LevelId).HasColumnName("level_id");

                entity.Property(e => e.PlayerId).HasColumnName("player_id");

                entity.Property(e => e.TimeInSeconds).HasColumnName("time_in_seconds");

                entity.Property(e => e.TimeStamp)
                    .HasColumnName("time_stamp")
                    .HasColumnType("timestamp with time zone");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
