﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Homework7.Models
{
    public partial class Level
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
