﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Homework7.Models
{
    public partial class Score
    {
        public int Id { get; set; }
        public DateTime? TimeStamp { get; set; }
        [Required]
        public int? TimeInSeconds { get; set; }
        [Required]
        public int? HighScore { get; set; }
        [Required]
        public int PlayerId { get; set; }
        public int? LevelId { get; set; }
    }
}
