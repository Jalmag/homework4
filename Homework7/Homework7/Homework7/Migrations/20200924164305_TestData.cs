﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Homework7.Migrations
{
    public partial class TestData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "levels",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    name = table.Column<string>(type: "character varying", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_levels", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "players",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    user_name = table.Column<string>(type: "character varying", nullable: false),
                    country_code = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_players", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "scores",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    time_stamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    time_in_seconds = table.Column<int>(nullable: false),
                    high_score = table.Column<int>(nullable: false),
                    player_id = table.Column<int>(nullable: false),
                    level_id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_scores", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "levels");

            migrationBuilder.DropTable(
                name: "players");

            migrationBuilder.DropTable(
                name: "scores");
        }
    }
}
