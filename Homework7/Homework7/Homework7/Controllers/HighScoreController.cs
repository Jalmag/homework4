﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Homework7.Models;
using Homework7.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Homework7.Controllers
{
    [ApiController]
    [Route("api")]
    public class HighScoreController : ControllerBase
    {
        private readonly IHighScoreRepository highScoreRepository;
        public HighScoreController(IHighScoreRepository repository)
        {
            highScoreRepository = repository;
        }
        [HttpGet]
        [Route("scores/level/{id:int}")]
        public IActionResult GetAllScores(int id)
        {
            List<Score> highScores = highScoreRepository.GetAllScores();
            highScores = highScores.Where(x => x.LevelId == id).ToList();
            return Ok(highScores);
        }

        [HttpGet]
        [Route("scores/player/{id:int}")]
        public IActionResult GetPlayerScores(int id)
        {
            List<Score> highScores = highScoreRepository.GetAllScores();
            highScores = highScores.Where(x => x.PlayerId == id).ToList();
            return Ok(highScores);
        }

        [HttpGet]
        [Route("scores/{id:int}")]
        public IActionResult GetScore(int id)
        {
            Score score = highScoreRepository.GetHighScore(id);
            return Ok(score);
        }

        [HttpGet]
        [Route("scores")]
        public IActionResult Get()
        {
            List<Score> highScores = highScoreRepository.GetAllScores();
            return Ok(highScores);
        }

        [HttpPost]
        [Route("scores/level/{id:int}")]
        public IActionResult PostScore(int id, [FromBody] Score score)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(score);
            }
            else
            {
                Score newScore = score;
                newScore.TimeStamp = DateTime.Now;
                newScore.LevelId = id;
                int newId = highScoreRepository.AddHighScore(newScore);
                return Created("scores/" + newId, newScore);
            }
        }

        [HttpDelete]
        [Route("scores/{id:int}")]
        public IActionResult DeleteScore(int id)
        {
            highScoreRepository.DeleteHighScore(id);
            return Ok();
        }

        [HttpGet]
        [Route ("players")]
        public IActionResult GetPlayers()
        {
            List<Player> allPlayers = highScoreRepository.GetAllPlayers();
            return Ok(allPlayers);
        }
        [HttpPost]
        [Route("players")]
        public IActionResult PostPlayer([FromBody] Player newPlayer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(newPlayer);
            }
            else
            {
                int newId = highScoreRepository.AddPlayer(newPlayer);
                return Created("players/" + newId, newPlayer);
            }
        }
        [HttpGet]
        [Route("levels")]
        public IActionResult GetAllLevels()
        {
            List<Level> allLevels = highScoreRepository.GetAllLevels();
            return Ok(allLevels);
        }
        [HttpPost]
        [Route("levels")]
        public IActionResult PostLevel([FromBody] Level newLevel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(newLevel);
            }
            else
            {
                int newId = highScoreRepository.AddLevel(newLevel);
                return Created("levels/" + newId, newLevel);
            }
        }
    }
}
